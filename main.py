import commands

PREFIX = "+"


class CommmandRegistry:
    def __init__(self):
        self.commands = []
        self.commands.append(commands.PollCommand())

    def command_exists(self, command: str) -> bool:
        for c in self.commands:
            if c.name == command:
                return True
            for alias in c.aliases:
                if alias == command:
                    return True
        return False

    def get_command(self, command: str) -> commands.Command:
        for c in self.commands:
            if c.name == command:
                return c
            for alias in c.aliases:
                if alias == command:
                    return c
        return None


if __name__ == "__main__":
    command_registry = CommmandRegistry()
    test_message = '+poll Lmao "1" "2" "3"'
    if test_message.startswith(PREFIX):
        command_str = test_message.split()[0][len(PREFIX):]
        if command_registry.command_exists(command_str):
            command_registry.get_command(command_str).process(
                test_message.removeprefix(PREFIX)
            )

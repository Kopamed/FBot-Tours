from abc import ABC, abstractmethod
import discord
from datetime import datetime


class Command(ABC):
    def __init__(self, name: str, alias: list, description: str):
        self.aliases = alias
        self.name = name
        self.description = description

    @abstractmethod
    def process(self, message: discord.Message):
        pass


class PollCommand(Command):
    def __init__(self):
        super(PollCommand, self).__init__("poll", ["pollcreate", "p"], "Makes a poll")

    def process(self, message: discord.Message): # todo: change str to dc.message
        # extract question
        content = message.content.removeprefix(message.content.split()[0] + " ")  # removing command
        question = content.split(' "')[0]
        poll = Poll(question, message.channel)
        options = content.split(' "')[1:]
        options = [i[:-1] for i in options]
        for index, option in enumerate(options):
            option = Option(option, str(index))
            poll.add_option(option)

        poll.send()


class Option:
    def __init__(self, name: str, emoji: str):
        self.name = name
        self.emoji = emoji
        self.reactants = []

    def add_reactant(self, reactant: discord.Member) -> None:
        self.reactants.append(reactant)

    def remove_reactant(self, reactant: discord.Member) -> None:
        self.reactants.remove(reactant)

    def reacts(self) -> int:
        return len(self.reactants)


class Poll:
    # send() # update()
    def __init__(self, title: str, channel: discord.TextChannel):
        self.title = title
        self.channel = channel
        self.options = []
        self.message = None

    def add_option(self, option: Option) -> None:
        self.options.append(option)

    def send(self) -> None:
        if self.message is None:
            embed = discord.Embed(color = 0x1ad2ea)
            embed.set_author(name=self.title)
            for option in self.options:
                embed.add_field(f":{option.emoji}: {option.name}")
            embed.set_footer(text="Poll created at " + datetime.today().strftime('%d/%m/%Y %H:%M'))
            self.message = self.channel.send(embed = embed)
        else:
            self.message.edit(content = "LMAOOOO")